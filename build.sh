#!/bin/bash
set -e

echo -n "Please enter new version: "
read version

echo "Building Linux binary..."
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o dbsync2_${version}

echo "Building Windows binary..."
GOOS=windows go build -o dbsync2_${version}.exe

echo "Building Mac binary..."
GOOS=darwin go build -o dbsync2_${version}_mac

# Use your Docker Hub namespace "dialfire"
IMAGE_NAME=dialfire/dbsync2

echo "Building Docker image..."
docker build -t ${IMAGE_NAME}:${version} -t ${IMAGE_NAME}:latest .

echo "Pushing Docker images..."
docker push ${IMAGE_NAME}:${version}
docker push ${IMAGE_NAME}:latest

echo "Build and upload completed successfully."
