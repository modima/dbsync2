# Builder stage
FROM golang:1.22 as builder
WORKDIR /go/src/app
RUN git clone https://bitbucket.org/modima/dbsync2.git /go/src/bitbucket.org/modima/dbsync2
WORKDIR /go/src/bitbucket.org/modima/dbsync2
RUN go mod tidy && \
    go build -a -installsuffix cgo -ldflags '-w -extldflags "-static"' -o /usr/local/bin/dbsync2 .

# Final stage with updated glibc (Debian Bookworm)
FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y ca-certificates && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/bin/dbsync2 /usr/local/bin/dbsync2
RUN chmod +x /usr/local/bin/dbsync2
ENTRYPOINT ["/usr/local/bin/dbsync2"]
